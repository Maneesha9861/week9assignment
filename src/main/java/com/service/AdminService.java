package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.Admin;
import com.bean.Books;
import com.dao.AdminDao;

@Service
public class AdminService {
	@Autowired
	AdminDao adminDao;
	
   public String adminRegistration(Admin admin) {
	   if(adminDao.existsById(admin.getEmail())) {
		   return " Details are Already Present";
	   }else {
		   adminDao.save(admin);
		   return "Details Saved Sucessfully";
	   }	   
   }
   
   public String checkLogin(Admin admin) {
	   if(adminDao.existsById(admin.getEmail())) {
		   Admin a=adminDao.getById(admin.getEmail());
		   if(a.getPassword().equals(admin.getPassword())) {
			   return "login sucessfull";
		   }else {
			   return "provide correct details";
		   }				   
	   }else {
		   return "sorry your details are not present";
	   }
	   
   }

   public List<Admin> getAllAdminAvaliable(){
		return adminDao.findAll();
	}
   
   public String logoutAdmin(String email) {
		if(!adminDao.existsById(email)) {
			return "invalid details ";
		}else {
			adminDao.deleteById(email);
			return "you have loged out";
		}
	}


   
}
