package com.bean;

import javax.persistence.Entity;

import javax.persistence.Id;

@Entity
public class Books {
	@Id
	private int bid;
	private String bname;
	private String genre;
	
	public int getBid() {
		return bid;
	}
	public void setBid(int bid) {
		this.bid = bid;
	}
	public String getBname() {
		return bname;
	}
	public void setBname(String bname) {
		this.bname = bname;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	@Override
	public String toString() {
		return "Books [bid=" + bid + ", bname=" + bname + ", genre=" + genre + "]";
	}


}
