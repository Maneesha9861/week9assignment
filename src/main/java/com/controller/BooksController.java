package com.controller;



import java.util.List;


import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bean.Books;
import com.service.BooksService;

@RestController
@RequestMapping("/books")
public class BooksController {
	
	@Autowired
	BooksService booksService;
	
		@GetMapping(value = "getAllBooks",
				    produces = MediaType.APPLICATION_JSON_VALUE)
		public List<Books> getAllBooks(){
			return booksService.getAllBooks();
		}

		@GetMapping(value = "getBookById/{bid}",
			    produces = MediaType.APPLICATION_JSON_VALUE)
	public Optional<Books> getBookById(@PathVariable("bid") int bid){
		return booksService.getBookById(bid);
	}
		@PostMapping(value = "storeBooks",
				consumes = MediaType.APPLICATION_JSON_VALUE)
		public String storeBooks(@RequestBody Books book) {
			
					return booksService.storeBook(book);
		}
		
		@PatchMapping(value = "updateBook")
		public String updateBookName(@RequestBody Books book) {
						return booksService.updateBookName(book);
		}
		
		@DeleteMapping(value = "deleteBook/{bid}")
		public String deleteBook(@PathVariable("bid") int bid) {
						return booksService.deleteBook(bid);
		}
		
}



